﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public static GameManager instance;
    public   Color [] levelColors;
    public int levelNo;
    public GameObject [] Levels;
    public Transform LevelParent;
    public ParticleSystem winParticle;
    public enum GameStates
    {
        Menu,GamePlay,Completed
    };
    public GameStates gameStates;
    void Awake()
    {
        instance = this;
    }
   

    // Update is called once per frame
    public void SpawnNewLevel()
    {
        if(LevelParent.childCount!=0)
        {
           Destroy( LevelParent.GetChild(0).gameObject);
        }
        GameObject go = Instantiate(Levels[PlayerPrefs.GetInt("level") % Levels.Length]);
        go.transform.parent = LevelParent;
        UiManager.Instance.SetLevelNo();
        PlayerController.instance.INIT();
    }
}
