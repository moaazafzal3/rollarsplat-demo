﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levelManager : MonoBehaviour
{
    public static levelManager instance;
   
    public Ground[] groundPieces;
    public int totalObjects;
    public int objectsCleared;
    
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.2f);
        setPieces();
        totalObjects = groundPieces.Length;
    }
    public void setPieces()
    {
        groundPieces = GameObject.FindObjectsOfType<Ground>();
    }
    public void checkLevel()
    {
        objectsCleared++;
        if (totalObjects == objectsCleared)
        {
            Debug.Log("cleared");
            UiManager.Instance.Completed();
        }

    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
