﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public static PlayerController instance;
    #region public Methods
    
    public float speed;
    public int minSwipeRecognition;

    #endregion

    private Rigidbody rb;
    public bool isMoving;
    public Vector3 MovingDirection;
    private Vector3 nextCollisionPosition;
    private Vector2 swipePostLastFrame;
    private Vector2 swipePostCurrentFrame;
    private Vector2 CurrentSwipe;
    public ParticleSystem particle;

 
    public float Position;
    private void Awake()
    {
        instance = this;
    }
    public  void INIT()
    {
        rb = GetComponent<Rigidbody>();
        
        GetComponent<MeshRenderer>().material.color = GameManager.instance.levelColors[GameManager.instance.levelNo%GameManager.instance.Levels.Length];
        particle.startColor= GameManager.instance.levelColors[GameManager.instance.levelNo % GameManager.instance.Levels.Length]; 
    }

    // Update is called once per frame
    private void Update()
    {
        if (GameManager.instance.gameStates == GameManager.GameStates.GamePlay)
        {
            if (isMoving)
            {
                rb.velocity = speed * MovingDirection * Time.deltaTime;
            }
            Collider[] hitColliders = Physics.OverlapSphere(transform.position - (Vector3.up / Position), 5f);
            int i = 0;
            while (i < hitColliders.Length)
            {
                Ground ground = hitColliders[i].transform.GetComponent<Ground>();

                if (ground && !ground.isColored)
                {
                    ground.ChangeColor(GameManager.instance.levelColors[GameManager.instance.levelNo % GameManager.instance.Levels.Length]);
                }

                i++;
            }
        }
    }
    void FixedUpdate()
    {
        if (GameManager.instance.gameStates == GameManager.GameStates.GamePlay)
        {
            if (nextCollisionPosition != Vector3.zero)
            {
                if (Vector3.Distance(transform.position, nextCollisionPosition) < 10)
                {
                    isMoving = false;
                    MovingDirection = Vector3.zero;
                    nextCollisionPosition = Vector3.zero;
                }
            }
            if (isMoving)
                return;
            Collider[] hitColliders = Physics.OverlapSphere(transform.position - (Vector3.up / Position), 2f);


            if (Input.GetMouseButton(0))
            {
                swipePostCurrentFrame = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

                if (swipePostLastFrame != Vector2.zero)
                {
                    CurrentSwipe = swipePostCurrentFrame - swipePostLastFrame;
                    if (CurrentSwipe.magnitude < minSwipeRecognition)
                    {
                        return;
                    }
                    CurrentSwipe.Normalize();
                    //  Debug.Log(CurrentSwipe);
                    if (CurrentSwipe.x > -0.5f && CurrentSwipe.x < 0.5f)
                    {

                        SetDestinationPosition(CurrentSwipe.y > 0 ? Vector3.forward : Vector3.back);
                    }
                    if (CurrentSwipe.y > -0.5f && CurrentSwipe.y < 0.5f)
                    {
                        SetDestinationPosition(CurrentSwipe.x > 0 ? Vector3.right : Vector3.left);

                    }

                }
                swipePostLastFrame = swipePostCurrentFrame;
            }
            if (Input.GetMouseButtonUp(0))
            {
                swipePostCurrentFrame = Vector2.zero;
                CurrentSwipe = Vector2.zero;
            }
        }
    }

    void SetDestinationPosition(Vector3 direction)
    {
        MovingDirection = direction;
        RaycastHit hit;
        if(Physics.Raycast(transform.position,direction,out hit)  )
        {
            nextCollisionPosition = hit.point;
        }
        isMoving = true;
    }
}
