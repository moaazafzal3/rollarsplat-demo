﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UiManager : MonoBehaviour
{
    public static UiManager Instance;
    // Start is called before the first frame update
    public GameObject mainMenuPanel, gamePlayPanel, completedPanel;
    public Text levelNo;
    void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        INIT();
        SetLevelNo();
        
    }
    private void INIT()
    {
       GameManager.instance. SpawnNewLevel();
    }

    // Update is called once per frame
    public void Completed()
    {
        StartCoroutine(completedDelay());
        GameManager.instance.winParticle.Play();

    }
    IEnumerator completedDelay()
    {
        yield return new WaitForSeconds(1.5f);
        completedPanel.SetActive(true);
        PlayerPrefs.SetInt("level", GameManager.instance.levelNo + 1);
        GameManager.instance.gameStates = GameManager.GameStates.Completed;
    }
    public void StartGame()
    {
        gamePlayPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
        GameManager.instance.gameStates = GameManager.GameStates.GamePlay;
    }
    public void NextLevel()
    {
        GameManager.instance.winParticle.Stop();

        completedPanel.SetActive(false);
        gamePlayPanel.SetActive(true);
        GameManager.instance.SpawnNewLevel();
        GameManager.instance.gameStates = GameManager.GameStates.GamePlay;
       


    }

    public void SetLevelNo()
    {
        levelNo.text= "LEVEL " + (PlayerPrefs.GetInt("level")+1);
        GameManager.instance.levelNo = (PlayerPrefs.GetInt("level"));
    }
}
